package PAQUETE;

import PAQUETE.LineaOrdenPK;
import PAQUETE.Orden;
import PAQUETE.Producto;
import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="EclipseLink-2.5.2.v20140319-rNA", date="2014-11-21T21:37:03")
@StaticMetamodel(LineaOrden.class)
public class LineaOrden_ { 

    public static volatile SingularAttribute<LineaOrden, Orden> orden;
    public static volatile SingularAttribute<LineaOrden, Integer> cantidad;
    public static volatile SingularAttribute<LineaOrden, Producto> producto;
    public static volatile SingularAttribute<LineaOrden, LineaOrdenPK> lineaOrdenPK;

}