package PAQUETE;

import PAQUETE.LineaOrden;
import java.util.Date;
import javax.annotation.Generated;
import javax.persistence.metamodel.CollectionAttribute;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="EclipseLink-2.5.2.v20140319-rNA", date="2014-11-21T21:37:03")
@StaticMetamodel(Orden.class)
public class Orden_ { 

    public static volatile SingularAttribute<Orden, Date> fechaOrden;
    public static volatile SingularAttribute<Orden, Integer> idOrden;
    public static volatile CollectionAttribute<Orden, LineaOrden> lineaOrdenCollection;

}