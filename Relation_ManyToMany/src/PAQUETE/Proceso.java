/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package PAQUETE;
import java.util.Collection;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
/**
 *
 * @author fer
 */
public class Proceso
{
    public Orden Insert_orden()
    {
        EntityManagerFactory emf = Persistence.createEntityManagerFactory("Relation_ManyToManyPU");
        EntityManager em = emf.createEntityManager();
        em.getTransaction().begin();
        
        Orden orden = new Orden();
        java.util.Date utilDate=new java.util.Date();
        java.sql.Date sqlDate=new java.sql.Date(utilDate.getTime());
        orden.setFechaOrden(sqlDate);
        try
        {
            em.persist(orden);
        }
        catch (Exception e)
        {
        }
        try
        {
            em.getTransaction().commit();
        }
        catch (Exception e)
        {
            System.out.println("ERROR" + e);
        }
        em.close();
        emf.close();
        return orden;
    }
    public Producto Insert_producto(String des,Float precio)
    {
        EntityManagerFactory emf = Persistence.createEntityManagerFactory("Relation_ManyToManyPU");
        EntityManager em = emf.createEntityManager();
        em.getTransaction().begin();
        Producto producto = new Producto();
        producto.setDescripcion(des);
        Float precioS=Float.valueOf(precio);
        producto.setPrecio(precio);
        try
        {
            em.persist(producto);
        }
        catch (Exception e)
        {System.out.println("error");}
        try
        {
            em.getTransaction().commit();
        }
        catch (Exception e)
        {
            System.out.println("ERROR" + e);
        }
        em.close();
        emf.close();
        return producto;
    }
    public LineaOrden Insert_lineaorden(Orden orden,Producto producto,int cantidad)
    {
        EntityManagerFactory emf = Persistence.createEntityManagerFactory("Relation_ManyToManyPU");
        EntityManager em = emf.createEntityManager();
        em.getTransaction().begin();
        
       
        LineaOrden lineaorden1 = new LineaOrden(orden.getIdOrden(),producto.getIdProducto());
        lineaorden1.setCantidad(cantidad);
        
        try
        {
            em.persist(lineaorden1);
        }
        catch (Exception e)
        {
            System.out.println("error");
        }
        try
        {
            em.getTransaction().commit();
        }
        catch (Exception e)
        {
 	           System.out.println("ERROR" + e);
        }
        em.close();
        emf.close();
        return lineaorden1;
    }
    public void Mostrar(Orden orden,Producto producto,LineaOrden lineaorden)
    {
        EntityManagerFactory emf = Persistence.createEntityManagerFactory("Relation_ManyToManyPU");
        EntityManager em = emf.createEntityManager();
        em.getTransaction().begin();
        Collection<Orden> lista_orden;
        lista_orden = em.createNamedQuery("Orden.findAll").getResultList();
        System.out.println("***************ORDEN*************************");
        for (Orden el : lista_orden)
        {
            System.out.println("ID orden: "+el.getIdOrden()+"\nFecha orden: "+el.getFechaOrden());
            System.out.println("--------------------------");
        
        }        
        System.out.println("***************PRODUCTO***************************");
        Collection<Producto> lista_producto;
        lista_producto = em.createNamedQuery("Producto.findAll").getResultList();

        for (Producto el : lista_producto)
        {
            System.out.println("ID producto: "+el.getIdProducto()+"\nDescripcion producto: "+el.getDescripcion()+"\nPrecio: "+el.getPrecio());
            System.out.println("--------------------------");
        }
        System.out.println("************************LINEA DE ORDEN*********************");
        Collection<LineaOrden> lista_lineaorden;
        lista_lineaorden = em.createNamedQuery("LineaOrden.findAll").getResultList();

        for (LineaOrden el : lista_lineaorden)
        {
            System.out.println("ID orden: "+el.getOrden().getIdOrden()+"\nID producto: "+el.getProducto().getIdProducto()+"\nCantidad: "+el.getCantidad()+
                    "\nPrecio Total: ");
      System.out.println("--------------------------");
        }
    }
}

































/*
        System.out.println("------------Datos Orden-------------");
        System.out.println("ID orden: "+orden.getIdOrden());
        System.out.println("ID orden: "+orden.getFechaOrden());
        System.out.println("------------Datos Producto----------");
        System.out.println("ID orden: "+producto.getIdProducto());
        System.out.println("ID orden: "+producto.getDescripcion());
        System.out.println("ID orden: "+producto.getPrecio());
        System.out.println("------------Datos Lineaorden--------");
        System.out.println("ID orden: "+lineaorden.getLineaordenPK().getIdOrden());
        System.out.println("ID orden: "+lineaorden.getLineaordenPK().getIdProducto());
        System.out.println("ID orden: "+lineaorden.getCantidad());
        */