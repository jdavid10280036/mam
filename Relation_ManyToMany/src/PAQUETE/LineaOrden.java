/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package PAQUETE;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

/**
 *
 * @author fer
 */
@Entity
@Table(name = "lineaOrden")
@NamedQueries({
    @NamedQuery(name = "LineaOrden.findAll", query = "SELECT l FROM LineaOrden l")})
public class LineaOrden implements Serializable {
    private static final long serialVersionUID = 1L;
    @EmbeddedId
    protected LineaOrdenPK lineaOrdenPK;
    @Column(name = "cantidad")
    private Integer cantidad;
    @JoinColumn(name = "idProducto", referencedColumnName = "idProducto", insertable = false, updatable = false)
    @ManyToOne(optional = false)
    private Producto producto;
    @JoinColumn(name = "idOrden", referencedColumnName = "idOrden", insertable = false, updatable = false)
    @ManyToOne(optional = false)
    private Orden orden;

    public LineaOrden() {
    }

    public LineaOrden(LineaOrdenPK lineaOrdenPK) {
        this.lineaOrdenPK = lineaOrdenPK;
    }

    public LineaOrden(int idOrden, int idProducto) {
        this.lineaOrdenPK = new LineaOrdenPK(idOrden, idProducto);
    }

    public LineaOrdenPK getLineaOrdenPK() {
        return lineaOrdenPK;
    }

    public void setLineaOrdenPK(LineaOrdenPK lineaOrdenPK) {
        this.lineaOrdenPK = lineaOrdenPK;
    }

    public Integer getCantidad() {
        return cantidad;
    }

    public void setCantidad(Integer cantidad) {
        this.cantidad = cantidad;
    }

    public Producto getProducto() {
        return producto;
    }

    public void setProducto(Producto producto) {
        this.producto = producto;
    }

    public Orden getOrden() {
        return orden;
    }

    public void setOrden(Orden orden) {
        this.orden = orden;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (lineaOrdenPK != null ? lineaOrdenPK.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof LineaOrden)) {
            return false;
        }
        LineaOrden other = (LineaOrden) object;
        if ((this.lineaOrdenPK == null && other.lineaOrdenPK != null) || (this.lineaOrdenPK != null && !this.lineaOrdenPK.equals(other.lineaOrdenPK))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "PAQUETE.LineaOrden[ lineaOrdenPK=" + lineaOrdenPK + " ]";
    }
    
}
