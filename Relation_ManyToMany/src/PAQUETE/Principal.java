/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package PAQUETE;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

/**
 *
 * @author fer
 */
public class Principal {
    public static void main(String[] args)throws IOException {
        int op=0;
        int cantidad=0;
        String descripcion,resp;
        float precio;        
        Proceso obj = new Proceso();
        Orden orden= new Orden();
        Producto producto = new Producto();
        LineaOrden lineaorden = new LineaOrden();
        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
        do
        {
           
            System.out.println("          MENU        ");
            System.out.println("1.- Insertar               ");
            System.out.println("2.- Desplegar              ");
            System.out.println("3.- Salir                  ");
 
            op=Integer.parseInt(br.readLine());
         
            switch (op)
            {
                case 1:
                    System.out.print("Ingresa el producto: ");
                    descripcion = br.readLine();
                    System.out.print("Ingresa el precio: ");
                    precio = Float.parseFloat(br.readLine());
                    System.out.print("Ingresa la cantidad: ");
                    cantidad = Integer.parseInt(br.readLine());
                    orden = obj.Insert_orden();
                    producto = obj.Insert_producto(descripcion, precio);
                    lineaorden = obj.Insert_lineaorden(orden, producto,cantidad);
                break;
                case 2:
                   
                    obj.Mostrar(orden, producto, lineaorden);
                    System.out.print("Presiona enter para regresar al menu");
                    resp=br.readLine();
                break;
            }
        }while(op!=3);

    }
    
}
